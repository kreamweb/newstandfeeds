<?php
/*
  Plugin Name: NewsStand Feeds
  Plugin URI: https://bitbucket.org/kreamweb/newstandfeeds	
  Description: Newsstand Atom Feed 1.2
  Author: Emanuela Castorina
  Author URI: http://www.kreamweb.com	
  Version: 1.0.0
  Disclaimer: Use at your own risk. No warranty expressed or implied is provided.
  License: GPL2
  
 */
 
DEFINE( "WPT_NSF_PLUGIN_PATH", plugin_dir_path(__FILE__)  );
require WPT_NSF_PLUGIN_PATH. 'newsstand_feeds_metaboxes.php';
 
function wpt_newsstand_feeds_admin_links( $links, $file ) {
	if ( $file == plugin_basename( dirname(__FILE__).'/newsstand_feeds.php' ) ) {
		$settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=newsstand-feeds/newsstand_feeds.php">'.__('Settings').'</a>';
		array_unshift($links, $settings_link);
	}
	return $links;
}
add_filter( 'plugin_action_links', 'wpt_newsstand_feeds_admin_links', 10, 2 );

add_action('admin_menu','wpt_newsstand_feeds_create_menu');
function wpt_newsstand_feeds_create_menu(){
	add_options_page('NewsStand Feeds', 'NewsStand Feeds','manage_options',__FILE__,'wpt_newsstand_feeds_settings_page');
	
	$options = array();
	$post_types=array('post');
	$options['post_types'] = $post_types;
    
	add_option('wpt_newsstand_feeds', $options, 'Options for the NewsStand Feeds plugin');
	
}

function wpt_newsstand_feeds_settings_page(){
	
	$types=get_post_types();
	$exclude_type=array('nav_menu_item', 'attachment','revision' );
	
	if (isset($_POST['submitted']) ) {
		$options = array();
        $options['post_types'] = $_POST['post_types'];
        update_option('wpt_newsstand_feeds', $options);
		echo '<div class="updated"><p>Plugin settings saved.</p></div>';
	}
	
	
	$options = get_option('wpt_newsstand_feeds');
	$post_types = $options['post_types'];
	$action_url = $_SERVER['PHP_SELF'] . '?page=newsstand-feeds/' . basename(__FILE__);

  echo <<<END
	<div class='wrap'>\n
		<h2>NewsStand Feeds Options</h2>\n
		<p>

		<form name="newsstand_feeds_form" action="$action_url" method="post">
		<input type="hidden" name="submitted" value="1" />
		<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="post_type">Choose post type</label></th>
			<td>
END;
			
			
if(!empty($types)){
	foreach($types as $type){
		if(!in_array($type, $exclude_type)){
			$t='';
			if(!empty($post_types) && in_array($type,$post_types)) $t='checked="checked"';
			echo '<br><input type="checkbox" name="post_types[]" value="'.$type.'" '.$t.'/> '.$type;
		}
	}
}
 echo '</td>
		</tr>
		<tr valign="top">
			<td>
				<input type="submit" name="save" value="Save Options" class="button-primary" />
				<input type="submit" name="reset" value="Reset" class="button-secondary" />
			</td>
		</tr>
		
		</table>';

	
}

function newsstand_feeds(){
	
	
}




function generate_atom(){
	echo('<?xml version="1.0" encoding="UTF-8"?>');
	echo('<feed xmlns="http://www.w3.org/2005/Atom" xmlns:news="http://itunes.apple.com/2011/Newsstand">');
	echo('<updated>2011-08-01T07:00:00Z</updated>');
?>	 
	<entry>  
	<id>613</id>
    <updated>2011-08-01T07:00:00Z</updated>
    <published>2011-08-01T07:00:00Z</published>
    <news:end_date>2011-08-08T07:00:00Z</news:end_date>
    <summary>In the latest issue, we cover the new Apple Newsstand API.</summary>
	<news:cover_art_icon size="IPHONE_SHELF_1X" src="http://images.asw.net/613/iphone_shelf.png"/>
    <news:cover_art_icon size="IPHONE_MULTITASKING_2X" src="http://images.asw.net/613/iphone_multitasking_2x.png"/>
    <news:cover_art_icon size="IPAD_SHELF_1X" src="http://images.asw.net/613/ipad_shelf.png"/>
    <news:cover_art_icon size="IPAD_SHELF_2X" src="http://images.asw.net/613/ipad_shelf_2x.png"/>
    <news:cover_art_icon size="IPHONE_SHELF_2X" src="http://images.asw.net/613/iphone_shelf_2x.png"/>
    </news:cover_art_icons>
    </entry>
<?php 	
	echo('</feed>');
}
